package pl.edu.pwr.pp;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.*;

public class ImageConverterTest {

    @Test
    public void shouldConvertIntensitiesToAscii(){
        //given
        int[][] intensities = {{10, 100},{200, 250}};
        char[][] expected = {{'@', '*'},{':',' '}};

        //when
        char[][] actual = ImageConverter.intensitiesToAscii(intensities);

        //then
        MatcherAssert.assertThat(actual[0][0], Matchers.is(expected[0][0]));
        MatcherAssert.assertThat(actual[0][1], Matchers.is(expected[0][1]));
        MatcherAssert.assertThat(actual[1][0], Matchers.is(expected[1][0]));
        MatcherAssert.assertThat(actual[1][1], Matchers.is(expected[1][1]));
    }

}