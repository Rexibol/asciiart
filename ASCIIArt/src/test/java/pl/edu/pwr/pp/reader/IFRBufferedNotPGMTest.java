package pl.edu.pwr.pp.reader;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.regex.Matcher;

public class IFRBufferedNotPGMTest {
    @Mock
    BufferedImage bufferedImage;

    IFRBufferedNotPGM testedObject;

    @Mock
    Color color;

    ArgumentCaptor<Color> captor;

    @Before
    public void metoda() {
        MockitoAnnotations.initMocks(this);
        testedObject = new IFRBufferedNotPGM();
    }

    @Test
    public void shouldLoadGreyPixel() {
        Mockito.when(bufferedImage.getWidth()).thenReturn(1);
        Mockito.when(bufferedImage.getHeight()).thenReturn(1);
        Mockito.when(bufferedImage.getType()).thenReturn(BufferedImage.TYPE_BYTE_GRAY);
        Mockito.when(bufferedImage.getRGB(0, 0)).thenReturn(5);

        testedObject.setBufferedImage(bufferedImage);
        testedObject.loadPixels();

        Assert.assertThat(testedObject.getPixels()[0][0], Matchers.is(Matchers.equalTo(5)));

    }

    @Test
    public void shouldLoadColorPixel() {
        Mockito.when(bufferedImage.getWidth()).thenReturn(1);
        Mockito.when(bufferedImage.getHeight()).thenReturn(1);
        Mockito.when(bufferedImage.getType()).thenReturn(BufferedImage.TYPE_INT_RGB);
        int color = new Color(100, 100, 100).getRGB();
        Mockito.when(bufferedImage.getRGB(0, 0)).thenReturn(color);
        captor = ArgumentCaptor.forClass(Color.class);
        IFRBufferedNotPGM spy = Mockito.spy(testedObject);
        Mockito.doCallRealMethod().when(spy).loadPixels();

        spy.setBufferedImage(bufferedImage);
        spy.loadPixels();

        Mockito.verify(spy).getIllumination(captor.capture());


        Assert.assertThat(captor.getValue().getRGB(), Matchers.is(Matchers.equalTo(color)));

    }

    @Test
    public void shouldCheckIfBufferedCorrupted_Yes(){
        Assert.assertThat(testedObject.isCorrupted(), Matchers.is(true));
    }

    @Test
    public void shouldCheckIfBufferedCorrupted_Nope(){
        testedObject.setIcon(new ImageIcon());
        Assert.assertThat(testedObject.isCorrupted(), Matchers.is(false));
    }
}