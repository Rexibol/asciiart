package pl.edu.pwr.pp.reader;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import pl.edu.pwr.pp.reader.IFRBufferedImage;
import pl.edu.pwr.pp.reader.IFRBufferedPGM;
import pl.edu.pwr.pp.reader.ImageFileReader;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ImageFileReaderTest {

    ImageFileReader imageReader;

    @Before
    public void setUp() {
        imageReader = ImageFileReader.create();
    }

    @Test
    public void shouldReadSequenceFrom0To255GivenTestImage() throws IOException {
        // given
        String fileName = "testImage.pgm";
        // when
        int[][] intensities = null;
        try {
            IFRBufferedImage image = new IFRBufferedPGM(fileName);
            intensities = image.getPixels();
        } catch (URISyntaxException e) {
            Assert.fail("Should read the file");
        }
        // then
        int counter = 0;
        for (int[] row : intensities) {
            for (int intensity : row) {
                assertThat(intensity, is(equalTo(counter++)));
            }
        }
    }

    @Test
    public void shouldThrowExceptionWhenFileDontExist() {
        // given
        String fileName = "nonexistent.pgm";
        try {
            // when
            IFRBufferedImage image = new IFRBufferedPGM(fileName);
            // then
            Assert.fail("Should throw exception");
        } catch (Exception e) {
            assertThat(e, is(instanceOf(NullPointerException.class)));
        }

    }

    @Test
    public void shouldGetExtension() throws MalformedURLException {
        //given
        String expected = "pp";
        URL input = new URL("file:///a/b/c/d/e." + expected);

        //when
        String actual = imageReader.getExtension(input);

        //then
        assertThat(actual, is(expected));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIfEmptyURL() throws MalformedURLException {
        //given
        URL input = new URL("file:///");

        //when
        String actual = imageReader.getExtension(input);
    }

    @Test
    public void shouldCheckIfPGM_pgm() throws MalformedURLException {
        //when
        String extension = "pgm";
        URL input = new URL("file:///");
        ImageFileReader spy = Mockito.spy(imageReader);

        //when
        Mockito.doReturn(extension).when(spy).getExtension(input);
        boolean actual = spy.isPGM(input);

        //then
        assertThat(actual, is(true));
    }

    @Test
    public void shouldCheckIfPGM_PGM() throws MalformedURLException {
        //when
        String extension = "PGM";
        URL input = new URL("file:///");
        ImageFileReader spy = Mockito.spy(imageReader);

        //when
        Mockito.doReturn(extension).when(spy).getExtension(input);
        boolean actual = spy.isPGM(input);

        //then
        assertThat(actual, is(true));
    }

    @Test
    public void shouldCheckIfPGM_jpg() throws MalformedURLException {
        //when
        String extension = "jpg";
        URL input = new URL("file:///");
        ImageFileReader spy = Mockito.spy(imageReader);

        //when
        Mockito.doReturn(extension).when(spy).getExtension(input);
        boolean actual = spy.isPGM(input);

        //then
        assertThat(actual, is(false));
    }

    @Test(expected = IOException.class)
    public void shouldThrowExceptionIfCorrupted() throws IOException, URISyntaxException {
        imageReader.createImageIcon("file:///giga");
    }


}
