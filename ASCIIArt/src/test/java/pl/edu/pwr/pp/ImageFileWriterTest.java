package pl.edu.pwr.pp;

import org.hamcrest.*;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.verification.VerificationMode;

import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.junit.Assert.*;

public class ImageFileWriterTest {

    @Mock
    private PrintWriter printWriter;

    private ImageFileWriter testedObject;

    @Captor
    private ArgumentCaptor<Character> captor;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        testedObject = new ImageFileWriter(printWriter);
    }

    @Test
    public void shouldSaveToFile(){
        //given
        char[][] input = {{'a', 'b'},{'c', 'd'},{'f', '0'}};

        //when
        testedObject.saveToTxtFile(input);

        //then
        Mockito.verify(printWriter, VerificationModeFactory.times(input.length*input[0].length)).print(captor.capture());
        Mockito.verify(printWriter, VerificationModeFactory.times(input.length)).println();
        Mockito.verify(printWriter, VerificationModeFactory.times(1)).close();

        List<Character> captured = captor.getAllValues();
        for(int i = 0; i < input.length; i++){
            for(int j = 0; j < input[i].length; j++){
                MatcherAssert.assertThat(input[i][j], Matchers.is(captured.get((input.length-1)*i+j)));
            }
        }
    }

}