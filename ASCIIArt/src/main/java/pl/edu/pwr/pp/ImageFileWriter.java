package pl.edu.pwr.pp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class ImageFileWriter {

    private final PrintWriter elementToWrite;

    public ImageFileWriter(String fileName) throws FileNotFoundException {
        elementToWrite = new PrintWriter(fileName);
    }

    ImageFileWriter(PrintWriter printWriter){
        this.elementToWrite = printWriter;
    }

    public void saveToTxtFile(char[][] ascii) {
        for (int i = 0; i < ascii.length; i++) {
            for (int j = 0; j < ascii[i].length; j++) {
                elementToWrite.print(new Character(ascii[i][j])); //opakowane na potrzeby testów
            }
            elementToWrite.println();//nowa linia
        }
        elementToWrite.close();
    }
}
