package pl.edu.pwr.pp.reader;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class IFRBufferedPGM extends IFRBufferedImage {

    public IFRBufferedPGM(URL url) throws IOException, URISyntaxException {
        super();
        loadPixels(url);
        createIcon();
    }

    public IFRBufferedPGM(String fileName) throws IOException, URISyntaxException {
        super();
        loadPixels(fileName);
        createIcon();
    }

    private void loadPixels(String fileName) throws URISyntaxException, IOException {
        readFromBuffer(Files.newBufferedReader(getPathToFile(fileName)));
    }

    private void loadPixels(URL imgURL) throws URISyntaxException, IOException {
        readFromBuffer(new BufferedReader(new InputStreamReader(imgURL.openStream())));
    }

    private void readFromBuffer(BufferedReader bufferedReader) {
        int columns = 0;
        int rows = 0;
        try (BufferedReader reader = bufferedReader) {

            reader.readLine();

            reader.readLine();

            String currentLine = reader.readLine();
            String colAndRow[] = currentLine.split(" ");

            columns = Integer.parseInt(colAndRow[0]);
            rows = Integer.parseInt(colAndRow[1]);

            reader.readLine();

            pixels = new int[rows][];

            for (int i = 0; i < rows; i++) {
                pixels[i] = new int[columns];
            }

            String line = null;
            int currentRow = 0;
            int currentColumn = 0;
            while ((line = reader.readLine()) != null) {
                String[] elements = line.split(" ");
                for (int i = 0; i < elements.length; i++) {
                    if (currentColumn < columns) {
                        pixels[currentRow][currentColumn] = Integer.parseInt(elements[i]);
                        currentColumn++;
                    } else {
                        currentRow++; //jedno w dol(bo to jest tablica)
                        currentColumn = 0; //(wracamy do elemetu erowego)
                        pixels[currentRow][currentColumn] = Integer.parseInt(elements[i]);
                        currentColumn++; //po dodaniu mozemy zwiekszyc o 1
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Path getPathToFile(String fileName) throws URISyntaxException {
        URI uri = ClassLoader.getSystemResource(fileName).toURI();
        return Paths.get(uri);
    }

    private void createIcon() throws IOException {
        BufferedImage image;
        int imgHeight = pixels.length;
        int imgWidth = pixels[0].length;
        image = new BufferedImage(imgWidth, imgHeight,
                BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster raster = image.getRaster();
        for (int y = 0; y < imgHeight; y++) {
            for (int x = 0; (x < imgWidth); x++) {
                raster.setSample(x, y, 0, pixels[y][x]);
            }
        }
        setIcon(image);
    }

}
