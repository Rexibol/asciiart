package pl.edu.pwr.pp;

import org.eclipse.wb.swing.FocusTraversalOnArray;
import pl.edu.pwr.pp.reader.ImageFileReader;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import static java.nio.file.Files.lines;

public class ASCIIEditorWindow {

    private JFrame frame;

    private static ImageFileReader imageFileReader;

    private JLabel lblMiejsceNaObrazek;
    private JButton btnZapiszDoPliku;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ASCIIEditorWindow window = new ASCIIEditorWindow();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public ASCIIEditorWindow() {
        initialize();
    }
    
    public JLabel getLblMiejsceNaObrazek() {
        return lblMiejsceNaObrazek;
    }

    public void setLblMiejsceNaObrazek(JLabel lblMiejsceNaObrazek) {
        this.lblMiejsceNaObrazek = lblMiejsceNaObrazek;
    }

    public void enableSave(boolean enabled){
        btnZapiszDoPliku.setEnabled(enabled);
    }

    public JFrame getFrame() {
        return frame;
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        imageFileReader = ImageFileReader.create();
        frame = new JFrame();
        frame.getContentPane().setBackground(new Color(175, 238, 238));
        frame.setBounds(100, 100, 581, 454);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{36, 97, 31, 352, 0};
        gridBagLayout.rowHeights = new int[]{17, 25, 25, 25, 25, 25, 25, 25, 0};
        gridBagLayout.columnWeights = new double[]{1.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
        frame.getContentPane().setLayout(gridBagLayout);

        JButton btnWczytajObraz = createWczytajObrazButton();

        createMiejsceNaObrazek();

        createOpcja1Checkbox();

        createOpcja2Checkbox();

        createZapiszDoPlikuButton();

        JButton btnFunkcja1 = createFunkcja1Button();

        JButton btnFunkcja2 = createFunkcja2Button();
        frame.getContentPane().setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{btnWczytajObraz, btnZapiszDoPliku, btnFunkcja1, btnFunkcja2}));
    }

	private JButton createFunkcja2Button() {
		JButton btnFunkcja2 = new JButton("[ Funkcja 2 ]");
        btnFunkcja2.setEnabled(false);
        GridBagConstraints gbc_btnFunkcja2 = new GridBagConstraints();
        gbc_btnFunkcja2.insets = new Insets(0, 0, 5, 5);
        gbc_btnFunkcja2.gridx = 1;
        gbc_btnFunkcja2.gridy = 6;
        frame.getContentPane().add(btnFunkcja2, gbc_btnFunkcja2);
		return btnFunkcja2;
	}

	private JButton createFunkcja1Button() {
		JButton btnFunkcja1 = new JButton("[ Funkcja 1 ]");
        btnFunkcja1.setEnabled(false);
        GridBagConstraints gbc_btnFunkcja1 = new GridBagConstraints();
        gbc_btnFunkcja1.insets = new Insets(0, 0, 5, 5);
        gbc_btnFunkcja1.gridx = 1;
        gbc_btnFunkcja1.gridy = 5;
        frame.getContentPane().add(btnFunkcja1, gbc_btnFunkcja1);
		return btnFunkcja1;
	}

	private void createZapiszDoPlikuButton() {
		btnZapiszDoPliku = new JButton("Zapisz do pliku");
        btnZapiszDoPliku.setEnabled(false);
        btnZapiszDoPliku.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                int returnValue = fileChooser.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    try {
                        ImageFileWriter writer = new ImageFileWriter(selectedFile.getCanonicalPath());
                        writer.saveToTxtFile(ImageConverter.intensitiesToAscii(imageFileReader.getBufferedImagePixels()));
                        JOptionPane.showMessageDialog(frame, "Zapisano!");
                    } catch (IOException e1) {
                        JOptionPane.showMessageDialog(frame, "Nie udało się zapisać do pliku!");
                    }
                }
            }
        });
        GridBagConstraints gbc_btnZapiszDoPliku = new GridBagConstraints();
        gbc_btnZapiszDoPliku.insets = new Insets(0, 0, 5, 5);
        gbc_btnZapiszDoPliku.gridx = 1;
        gbc_btnZapiszDoPliku.gridy = 4;
        frame.getContentPane().add(btnZapiszDoPliku, gbc_btnZapiszDoPliku);
	}

	private void createOpcja2Checkbox() {
		JCheckBox chckbxOpcja_1 = new JCheckBox("Opcja 2");
        chckbxOpcja_1.setEnabled(false);
        GridBagConstraints gbc_chckbxOpcja_1 = new GridBagConstraints();
        gbc_chckbxOpcja_1.insets = new Insets(0, 0, 5, 5);
        gbc_chckbxOpcja_1.gridx = 1;
        gbc_chckbxOpcja_1.gridy = 3;
        frame.getContentPane().add(chckbxOpcja_1, gbc_chckbxOpcja_1);
	}

	private void createOpcja1Checkbox() {
		JCheckBox chckbxOpcja = new JCheckBox("Opcja 1");
        chckbxOpcja.setEnabled(false);
        GridBagConstraints gbc_chckbxOpcja = new GridBagConstraints();
        gbc_chckbxOpcja.insets = new Insets(0, 0, 5, 5);
        gbc_chckbxOpcja.gridx = 1;
        gbc_chckbxOpcja.gridy = 2;
        frame.getContentPane().add(chckbxOpcja, gbc_chckbxOpcja);
	}

	private void createMiejsceNaObrazek() {
		lblMiejsceNaObrazek = new JLabel();
        lblMiejsceNaObrazek.setBackground(new Color(255, 248, 220));
        GridBagConstraints gbc_lblMiejsceNaObrazek = new GridBagConstraints();
        gbc_lblMiejsceNaObrazek.fill = GridBagConstraints.VERTICAL;
        gbc_lblMiejsceNaObrazek.gridheight = 8;
        gbc_lblMiejsceNaObrazek.gridx = 3;
        gbc_lblMiejsceNaObrazek.gridy = 0;
        frame.getContentPane().add(lblMiejsceNaObrazek, gbc_lblMiejsceNaObrazek);
	}

	private JButton createWczytajObrazButton() {
		JButton btnWczytajObraz = new JButton("Wczytaj obraz");
        ASCIIEditorWindow me = this;
        btnWczytajObraz.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                OknoDialogoweWczytajPlik oknoDialogoweWczytajPlik = new OknoDialogoweWczytajPlik(me);
                oknoDialogoweWczytajPlik.setVisible(true);
            }
        });
        GridBagConstraints gbc_btnWczytajObraz = new GridBagConstraints();
        gbc_btnWczytajObraz.insets = new Insets(0, 0, 5, 5);
        gbc_btnWczytajObraz.gridx = 1;
        gbc_btnWczytajObraz.gridy = 1;
        frame.getContentPane().add(btnWczytajObraz, gbc_btnWczytajObraz);
		return btnWczytajObraz;
	}
}
