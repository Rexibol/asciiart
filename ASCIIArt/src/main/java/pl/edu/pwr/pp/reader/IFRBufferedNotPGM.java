package pl.edu.pwr.pp.reader;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

public class IFRBufferedNotPGM extends IFRBufferedImage{

    private BufferedImage bufferedImage;

    public IFRBufferedNotPGM(URL url) throws IOException {
        super();
        createReader(url);
        loadPixels();
        createIcon();
    }

    IFRBufferedNotPGM(){}

    void setBufferedImage(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }

    void loadPixels(){
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        int type =  bufferedImage.getType();
        pixels = new int[height][width];
        for(int x = 0; x < width; x++){
            for(int y = 0; y < height; y++){
                if(type == BufferedImage.TYPE_BYTE_GRAY)
                    pixels[y][x] = bufferedImage.getRGB(x, y);
                else {
                    Color color = new Color(bufferedImage.getRGB(x, y));
                    pixels[y][x] = getIllumination(color);
                }
            }
        }
    }

    int getIllumination(Color color){
        return (int) (color.getRed()*0.2989 + color.getGreen()*0.5870 + color.getBlue()*0.1140);
    }

    private void createIcon() throws IOException {
        setIcon(bufferedImage);
    }

    private void createReader(URL url) throws IOException {
        bufferedImage = ImageIO.read(url);
    }

}
