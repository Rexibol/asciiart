package pl.edu.pwr.pp;

import pl.edu.pwr.pp.reader.ImageFileReader;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class OknoDialogoweWczytajPlik extends JDialog {

    private JPanel contentPane;
    private JTextField textFieldZDysku;
    private JTextField textFieldZAdresuURL;
    private final ASCIIEditorWindow asciiEditorWindow;
    private final ImageFileReader imageFileReader;

    /**
     * Create the frame.
     */
    public OknoDialogoweWczytajPlik(ASCIIEditorWindow asciiEditorWindow) {
        super(asciiEditorWindow.getFrame(), true);
        this.asciiEditorWindow = asciiEditorWindow;
        imageFileReader = ImageFileReader.create();
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
        gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
        gbl_contentPane.columnWeights = new double[]{0.0, 1.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_contentPane.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
        contentPane.setLayout(gbl_contentPane);

        JRadioButton rdbtnZDysku = new JRadioButton("Z dysku");
        GridBagConstraints gbc_rdbtnZDysku = new GridBagConstraints();
        gbc_rdbtnZDysku.insets = new Insets(0, 0, 5, 5);
        gbc_rdbtnZDysku.gridx = 1;
        gbc_rdbtnZDysku.gridy = 1;
        contentPane.add(rdbtnZDysku, gbc_rdbtnZDysku);

        JButton btnWybierzPlik = new JButton("Wybierz plik");
        btnWybierzPlik.addActionListener(e-> {
                JFileChooser fileChooser = new JFileChooser();
                int returnValue = fileChooser.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    try {
                        textFieldZDysku.setText(selectedFile.getCanonicalPath());
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }

       );

        GridBagConstraints gbc_btnWybierzPlik = new GridBagConstraints();
        gbc_btnWybierzPlik.insets = new Insets(0, 0, 5, 0);
        gbc_btnWybierzPlik.gridx = 4;
        gbc_btnWybierzPlik.gridy = 1;
        contentPane.add(btnWybierzPlik, gbc_btnWybierzPlik);

        textFieldZDysku = new JTextField();
        textFieldZDysku.setFont(new Font("Tahoma", Font.PLAIN, 13));
        GridBagConstraints gbc_textFieldZDysku = new GridBagConstraints();
        gbc_textFieldZDysku.gridwidth = 4;
        gbc_textFieldZDysku.insets = new Insets(0, 0, 5, 0);
        gbc_textFieldZDysku.fill = GridBagConstraints.HORIZONTAL;
        gbc_textFieldZDysku.gridx = 1;
        gbc_textFieldZDysku.gridy = 2;
        contentPane.add(textFieldZDysku, gbc_textFieldZDysku);
        textFieldZDysku.setColumns(10);

        JRadioButton rdbtnZAdresuUrl = new JRadioButton("Z adresu URL");
        GridBagConstraints gbc_rdbtnZAdresuUrl = new GridBagConstraints();
        gbc_rdbtnZAdresuUrl.insets = new Insets(0, 0, 5, 5);
        gbc_rdbtnZAdresuUrl.gridx = 1;
        gbc_rdbtnZAdresuUrl.gridy = 3;
        contentPane.add(rdbtnZAdresuUrl, gbc_rdbtnZAdresuUrl);

        ButtonGroup radios = new ButtonGroup();
        radios.add(rdbtnZDysku);
        radios.add(rdbtnZAdresuUrl);
        radios.setSelected(rdbtnZDysku.getModel(), true);

        textFieldZAdresuURL = new JTextField();
        GridBagConstraints gbc_textFieldZAdresuURL = new GridBagConstraints();
        gbc_textFieldZAdresuURL.insets = new Insets(0, 0, 5, 0);
        gbc_textFieldZAdresuURL.gridwidth = 4;
        gbc_textFieldZAdresuURL.fill = GridBagConstraints.HORIZONTAL;
        gbc_textFieldZAdresuURL.gridx = 1;
        gbc_textFieldZAdresuURL.gridy = 4;
        contentPane.add(textFieldZAdresuURL, gbc_textFieldZAdresuURL);
        textFieldZAdresuURL.setColumns(10);

        JButton btnOk = new JButton("Ok");
        JDialog me = this;
        btnOk.addActionListener(e-> {
                if (radios.isSelected(rdbtnZDysku.getModel())) {
                    if (textFieldZDysku.getText().length() == 0)
                        JOptionPane.showMessageDialog(me, "Nie podano lokalizacji pliku!");
                    else {
                        try {
                            String uri = "file:///" + textFieldZDysku.getText();
                            imageFileReader.createImageIcon(uri);
                            asciiEditorWindow.getLblMiejsceNaObrazek().setIcon(imageFileReader.getBufferedImageIcon());
                        } catch (IOException | URISyntaxException e1) {
                            JOptionPane.showMessageDialog(me, "Nie udało się odczytać pliku!");
                            return;
                        }
                        asciiEditorWindow.enableSave(true);
                        dispose();
                    }
                } else if (radios.isSelected(rdbtnZAdresuUrl.getModel())) {
                    if (textFieldZAdresuURL.getText().length() == 0)
                        JOptionPane.showMessageDialog(me, "Nie podano lokalizacji pliku!");
                    else {
                        try {
                            String uri = textFieldZAdresuURL.getText();
                            imageFileReader.createImageIcon(uri);
                            asciiEditorWindow.getLblMiejsceNaObrazek().setIcon(imageFileReader.getBufferedImageIcon());
                        } catch (IOException | URISyntaxException e1) {
                            JOptionPane.showMessageDialog(me, "Nie udało się odczytać pliku!");
                            return;
                        }
                        asciiEditorWindow.enableSave(true);
                        dispose();
                    }
                }
            });
        GridBagConstraints gbc_btnOk = new GridBagConstraints();
        gbc_btnOk.insets = new Insets(0, 0, 0, 5);
        gbc_btnOk.gridx = 1;
        gbc_btnOk.gridy = 6;
        contentPane.add(btnOk, gbc_btnOk);

        JButton btnAnuluj = new JButton("Anuluj");
        btnAnuluj.addActionListener(e-> {
                dispose();
            });
        GridBagConstraints gbc_btnAnuluj = new GridBagConstraints();
        gbc_btnAnuluj.gridx = 4;
        gbc_btnAnuluj.gridy = 6;
        contentPane.add(btnAnuluj, gbc_btnAnuluj);

    }

}
