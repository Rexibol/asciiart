package pl.edu.pwr.pp.reader;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class IFRBufferedImage {

    protected int[][] pixels;

    protected ImageIcon icon;

    public int[][] getPixels() {
        return pixels;
    }

    public ImageIcon getIcon() {
        return icon;
    }

    void setIcon(ImageIcon icon){
        this.icon = icon;
    }

    public boolean isCorrupted(){
        if(icon == null)
            return true;
        return false;
    }

    protected void setIcon(BufferedImage image){
        int width = 300;
        int height = countHeight(width, image);
        Image img = image.getScaledInstance(width, height, 0);
        icon = new ImageIcon(img);
    }

    private int countHeight(int width, BufferedImage image) {
        return (int) (((double) width) / ((double) image.getWidth()) * image.getHeight());
    }
}
