package pl.edu.pwr.pp.reader;

import javax.swing.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class ImageFileReader {

    private static ImageFileReader instance;

    private IFRBufferedImage bufferedImage;

    private ImageFileReader(){}

    public static ImageFileReader create(){
        if(instance == null){
            instance = new ImageFileReader();
        }
        return instance;
    }

    public int[][] getBufferedImagePixels() {
        return bufferedImage.getPixels();
    }

    public ImageIcon getBufferedImageIcon() {
        return bufferedImage.getIcon();
    }

    public boolean isPGM(URL imgURL) {
        return "pgm".equals(getExtension(imgURL)) || "PGM".equals(getExtension(imgURL));
    }
    
    public ImageIcon createImageIcon(String path) throws IOException, URISyntaxException {
        URL imgURL = new URL(path);
        int width = 300;
        if (isPGM(imgURL)) {
            bufferedImage = new IFRBufferedPGM(imgURL);
        } else {
            bufferedImage = new IFRBufferedNotPGM(imgURL);
        }
        if (bufferedImage.isCorrupted())
            throw new IOException();
        return bufferedImage.getIcon();
    }

    String getExtension(URL imgURL) {
        String[] pathElements = imgURL.getFile().split("/");
        if(pathElements.length == 0)
            throw new IllegalArgumentException();
        String[] nameElements = pathElements[pathElements.length - 1].split("\\.");
        return nameElements[nameElements.length - 1];
    }

}
